package net.lafox.adviqo.challenge.service;

import java.util.List;

import net.lafox.adviqo.challenge.dto.ExpertsSearchResult;


public interface ExpertService {
    ExpertsSearchResult fetchExperts(String name, String language, String description, List<String> sortCriteriaStings);
}

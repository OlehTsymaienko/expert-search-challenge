package net.lafox.adviqo.challenge.service.ds;


import static lombok.AccessLevel.PRIVATE;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@RequiredArgsConstructor(staticName = "of")
@FieldDefaults(level = PRIVATE)

public class SortCriteria {
    final String field;
    final String direction;

    public static SortCriteria of(String sortCriteriaString) {
        if (sortCriteriaString == null) {
            throw new IllegalArgumentException("SortCriteria string must have format fieldName:asc or fieldName:desc");
        }

        sortCriteriaString = sortCriteriaString.trim();

        if (sortCriteriaString.isEmpty() || ":".equals(sortCriteriaString)) {
            throw new IllegalArgumentException("SortCriteria string must have format fieldName:asc or fieldName:desc");
        }

        if (!sortCriteriaString.contains(":")) {
            sortCriteriaString += ":asc";
        }
        String[] split = sortCriteriaString.split(":");
        String field = split[0];
        String direction = split.length > 1 && "desc".equalsIgnoreCase(split[1]) ? "desc" : "asc";
        return SortCriteria.of(field, direction);
    }
}

package net.lafox.adviqo.challenge.service.ds;

public enum ComparisonType {
    EQUAL, LIKE
}

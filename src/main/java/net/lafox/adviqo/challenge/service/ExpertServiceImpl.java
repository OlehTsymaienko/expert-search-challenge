package net.lafox.adviqo.challenge.service;

import static java.util.stream.Collectors.toList;
import static net.lafox.adviqo.challenge.utils.StreamHelper.nullSafeStream;
import static org.springframework.util.StringUtils.isEmpty;

import java.util.ArrayList;
import java.util.List;

import net.lafox.adviqo.challenge.dto.Expert;
import net.lafox.adviqo.challenge.dto.ExpertsSearchResult;
import net.lafox.adviqo.challenge.persist.ExpertDao;
import net.lafox.adviqo.challenge.service.ds.ComparisonType;
import net.lafox.adviqo.challenge.service.ds.FilterCriteria;
import net.lafox.adviqo.challenge.service.ds.SortCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ExpertServiceImpl implements ExpertService {
    private final ExpertDao expertDao;

    @Autowired
    public ExpertServiceImpl(ExpertDao expertDao) {
        this.expertDao = expertDao;
    }

    @Override
    public ExpertsSearchResult fetchExperts(String name, String language, String description, List<String> sortCriteriaStings) {
        List<SortCriteria> sortCriteria = parseSortCriteriaStrings(sortCriteriaStings);
        List<FilterCriteria> filterCriteria = populateFilter(name, language, description);
        List<Expert> experts = expertDao.fetchExperts(filterCriteria, sortCriteria);
        return ExpertsSearchResult.of(experts);
    }

    private List<FilterCriteria> populateFilter(String name, String language, String description) {
        List<FilterCriteria> filterCriteria = new ArrayList<>();
        if (name != null) {
            filterCriteria.add(FilterCriteria.of("name", name, ComparisonType.EQUAL));
        }
        if (language != null) {
            filterCriteria.add(FilterCriteria.of("language", language, ComparisonType.EQUAL));
        }
        if (description != null) {
            filterCriteria.add(FilterCriteria.of("description", description, ComparisonType.LIKE));
        }
        return filterCriteria;
    }

    private List<SortCriteria> parseSortCriteriaStrings(List<String> sortCriteriaStings) {
        return nullSafeStream(sortCriteriaStings)
                .filter(x -> !isEmpty(x))
                .map(SortCriteria::of).collect(toList());
    }

}

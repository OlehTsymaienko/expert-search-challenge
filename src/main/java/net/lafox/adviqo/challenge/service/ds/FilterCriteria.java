package net.lafox.adviqo.challenge.service.ds;


import static lombok.AccessLevel.PRIVATE;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@RequiredArgsConstructor(staticName = "of")
@FieldDefaults(level = PRIVATE)

public class FilterCriteria {
    final String fieldName;
    final Object value;
    final ComparisonType comparisonType;
}

package net.lafox.adviqo.challenge.utils;

import java.util.Collection;
import java.util.stream.Stream;

public class StreamHelper {
    public static <T> Stream<T> nullSafeStream(Collection<T> collection) {
        return collection == null ? Stream.empty() : collection.stream();
    }
}

package net.lafox.adviqo.challenge.dto;

import static lombok.AccessLevel.PRIVATE;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
public class ExpertsSearchResult {
    List<Expert> experts;

    public static ExpertsSearchResult of(List<Expert> experts) {
        return new ExpertsSearchResult(experts);
    }
}

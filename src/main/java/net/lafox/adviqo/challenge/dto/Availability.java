package net.lafox.adviqo.challenge.dto;

public enum Availability {
    ONLINE, OFFLINE, BUSY
}

package net.lafox.adviqo.challenge.persist.hibernate.entity;

import static lombok.AccessLevel.PRIVATE;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import net.lafox.adviqo.challenge.dto.Availability;

@Data
@NoArgsConstructor
@FieldDefaults(level = PRIVATE)

@Entity
@Table(name = "experts")
public class ExpertEntity {
    @Id
    @GeneratedValue
    Long id;

    @Column(length = 63, nullable = false)
    String name;

    @Column(length = 65535)
    String description;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    Availability availability;

    @Column(nullable = false)
    double pricePerMinute;

    @Column(nullable = false)
    String language;
}

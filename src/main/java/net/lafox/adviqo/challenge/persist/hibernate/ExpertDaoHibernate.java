package net.lafox.adviqo.challenge.persist.hibernate;

import static java.util.stream.Collectors.toList;
import static net.lafox.adviqo.challenge.utils.StreamHelper.nullSafeStream;

import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import net.lafox.adviqo.challenge.dto.Expert;
import net.lafox.adviqo.challenge.persist.hibernate.entity.ExpertEntity;
import net.lafox.adviqo.challenge.persist.ExpertDao;
import net.lafox.adviqo.challenge.service.ds.FilterCriteria;
import net.lafox.adviqo.challenge.service.ds.SortCriteria;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class ExpertDaoHibernate implements ExpertDao {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Expert> fetchExperts(List<FilterCriteria> filterCriteria, List<SortCriteria> sortCriteria) {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<ExpertEntity> query = cb.createQuery(ExpertEntity.class);
        Root<ExpertEntity> root = query.from(ExpertEntity.class);

        Predicate[] filters = nullSafeStream(filterCriteria)
                .map(criteria -> createPredicate(cb, root, criteria))
                .filter(Objects::nonNull)
                .toArray(Predicate[]::new);

        Order[] orders = nullSafeStream(sortCriteria)
                .map(criteria -> toOrder(cb, root, criteria))
                .toArray(Order[]::new);

        query.where(filters);
        query.orderBy(orders);

        List<ExpertEntity> resultList = em.createQuery(query).getResultList();

        return resultList.stream().map(this::toExpert).collect(toList());
    }

    private Order toOrder(CriteriaBuilder cb, Root<ExpertEntity> root, SortCriteria criteria) {
        if ("ASC".equalsIgnoreCase(criteria.getDirection())) {
            return cb.asc(root.get(criteria.getField()));
        } else {
            return cb.desc(root.get(criteria.getField()));
        }
    }

    private Predicate createPredicate(CriteriaBuilder cb, Root<ExpertEntity> expert, FilterCriteria criteria) {
        switch (criteria.getComparisonType()) {
            case EQUAL:
                return cb.equal(expert.get(criteria.getFieldName()), criteria.getValue());
            case LIKE:
                return cb.like(expert.get(criteria.getFieldName()), "%" + criteria.getValue() + "%");
        }
        return null;
    }

    private Expert toExpert(ExpertEntity entity) {
        return Expert.builder()
                .name(entity.getName())
                .description(entity.getDescription())
                .language(entity.getLanguage())
                .pricePerMinute(entity.getPricePerMinute())
                .availability(entity.getAvailability())
                .build();
    }
}

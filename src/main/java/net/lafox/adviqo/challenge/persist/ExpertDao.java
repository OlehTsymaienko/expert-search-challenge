package net.lafox.adviqo.challenge.persist;


import java.util.List;

import net.lafox.adviqo.challenge.dto.Expert;
import net.lafox.adviqo.challenge.service.ds.FilterCriteria;
import net.lafox.adviqo.challenge.service.ds.SortCriteria;

public interface ExpertDao {
    List<Expert> fetchExperts(List<FilterCriteria> filterCriteria, List<SortCriteria> sortCriteria);
}

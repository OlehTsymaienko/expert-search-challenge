package net.lafox.adviqo.challenge.rest;

import java.util.List;

import net.lafox.adviqo.challenge.dto.ExpertsSearchResult;
import net.lafox.adviqo.challenge.service.ExpertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExpertsController {

    private final ExpertService expertService;

    @Autowired
    public ExpertsController(ExpertService expertService) {
        this.expertService = expertService;
    }

    @GetMapping(value = "experts", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<ExpertsSearchResult> experts(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String language,
            @RequestParam(required = false) String description,
            @RequestParam(required = false) List<String> sortCriteria
    ) {
        ExpertsSearchResult result = expertService.fetchExperts(name, language, description, sortCriteria);
        if (result.getExperts().isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }

}


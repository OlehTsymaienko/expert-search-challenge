insert into experts (id, availability, description, language, name, price_per_minute) VALUES (21, 'ONLINE',  'good boy 1', 'German' ,  'Philip_1', 1);
insert into experts (id, availability, description, language, name, price_per_minute) VALUES (22, 'ONLINE',  'good boy 2', 'English',  'Oleh_1',   2);
insert into experts (id, availability, description, language, name, price_per_minute) VALUES (23, 'ONLINE',  'good boy 3', 'French' ,  'Alice_1',  3);
insert into experts (id, availability, description, language, name, price_per_minute) VALUES (24, 'ONLINE',  'good boy 4', 'English',  'MAx_1',    4);

insert into experts (id, availability, description, language, name, price_per_minute) VALUES (25, 'OFFLINE', 'bad boy 1', 'German' ,  'Philip_2', 1);
insert into experts (id, availability, description, language, name, price_per_minute) VALUES (26, 'OFFLINE', 'bad boy 2', 'English',  'Oleh_2',   2);
insert into experts (id, availability, description, language, name, price_per_minute) VALUES (27, 'OFFLINE', 'bad boy 3', 'French' ,  'Alice_2',  3);
insert into experts (id, availability, description, language, name, price_per_minute) VALUES (28, 'OFFLINE', 'bad boy 4', 'English',  'MAx_2',    4);

insert into experts (id, availability, description, language, name, price_per_minute) VALUES (29,  'BUSY', 'nice boy 1', 'German' ,  'Philip_3', 1);
insert into experts (id, availability, description, language, name, price_per_minute) VALUES (210, 'BUSY', 'nice boy 2', 'English',  'Oleh_3',   2);
insert into experts (id, availability, description, language, name, price_per_minute) VALUES (211, 'BUSY', 'nice boy 3', 'French' ,  'Alice_3',  3);
insert into experts (id, availability, description, language, name, price_per_minute) VALUES (212, 'BUSY', 'nice boy 4', 'English',  'MAx_3',    4);

package net.lafox.adviqo.challenge.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import net.lafox.adviqo.challenge.service.ds.SortCriteria;
import org.junit.jupiter.api.Test;

class SortCriteriaTest {

    @Test
    void parseSortCriteriaString() {
        assertThrows(IllegalArgumentException.class, () -> SortCriteria.of(null));
        assertThrows(IllegalArgumentException.class, () -> SortCriteria.of(""));
        assertThrows(IllegalArgumentException.class, () -> SortCriteria.of(":"));
        assertThrows(IllegalArgumentException.class, () -> SortCriteria.of("   "));

        assertSortCriteria("name", "asc", SortCriteria.of("name"));
        assertSortCriteria("name", "asc", SortCriteria.of("name:"));
        assertSortCriteria("name", "asc", SortCriteria.of("name:asc"));
        assertSortCriteria("name", "asc", SortCriteria.of("name:aSc"));
        assertSortCriteria("name", "asc", SortCriteria.of("name:BAD_DIRECTION"));
        assertSortCriteria("name", "desc", SortCriteria.of("name:desc"));
        assertSortCriteria("name", "desc", SortCriteria.of("name:DeSc"));
    }

    private void assertSortCriteria(String fieldName, String direction, SortCriteria a) {
        assertNotNull(a, "SortCriteria");
        assertEquals(fieldName, a.getField(), "Field Name");
        assertEquals(direction, a.getDirection(), "Direction");
    }
}
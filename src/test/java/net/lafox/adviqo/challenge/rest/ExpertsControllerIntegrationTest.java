package net.lafox.adviqo.challenge.rest;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_XML;

import java.util.Collections;
import java.util.List;

import net.lafox.adviqo.challenge.dto.Expert;
import net.lafox.adviqo.challenge.dto.ExpertsSearchResult;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class ExpertsControllerIntegrationTest {

    @Autowired
    TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    @Test
    @DisplayName("GET JSON  http://localhost:8080/experts")
    void fetchAllAsJson() {
        ResponseEntity<ExpertsSearchResult> result = query("http://localhost:" + port + "/experts", APPLICATION_JSON);
        assertEquals("application/json", result.getHeaders().get("Content-Type").stream().findFirst().orElse(null));
    }

    @Test
    @DisplayName("GET XML  http://localhost:8080/experts")
    void fetchAllAsXml() {
        ResponseEntity<ExpertsSearchResult> result = query("http://localhost:" + port + "/experts", APPLICATION_XML);
        assertEquals("application/xml", result.getHeaders().get("Content-Type").stream().findFirst().orElse(null));
    }

    @Test
    @DisplayName("GET http://localhost:8080/experts")
    void fetchAllNoSortingNoFilters() {
        ResponseEntity<ExpertsSearchResult> result = query();

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertNotNull(result.getBody());
        assertNotNull(result.getBody().getExperts());
        List<Expert> experts = result.getBody().getExperts();
        assertEquals(12, experts.size());
    }

    //Experts by name
    @Test
    @DisplayName("GET http://localhost:8080/experts?name=Alice_1")
    void filterByName() {
        HttpHeaders params = new HttpHeaders();
        params.add("name", "Alice_1");
        ResponseEntity<ExpertsSearchResult> result = query(params);

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertNotNull(result.getBody());
        assertNotNull(result.getBody().getExperts());
        List<Expert> experts = result.getBody().getExperts();
        assertEquals(1, experts.size());
        assertEquals(asList("Alice_1"), experts.stream().map(Expert::getName).collect(toList()));
    }

    //Experts by language with sorting on the attribute’s availability and/or price
    @Test
    @DisplayName("GET http://localhost:8080/experts?language=French&sortCriteria=availability:asc,pricePerMinute:desc")
    void filterByLanguageOrderByAvailabilityAndPrice() {
        HttpHeaders params = new HttpHeaders();
        params.add("language", "French");
        params.add("sortCriteria", "availability:asc,pricePerMinute:desc");
        ResponseEntity<ExpertsSearchResult> result = query(params);

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertNotNull(result.getBody());
        assertNotNull(result.getBody().getExperts());
        List<Expert> experts = result.getBody().getExperts();
        assertEquals(3, experts.size());
        assertEquals(asList("Alice_3", "Alice_2", "Alice_1"), experts.stream().map(Expert::getName).collect(toList()));
    }

    //Experts by textual match in the description
    @Test
    @DisplayName("GET http://localhost:8080/experts?description=nice&sortCriteria=name")
    void filterByTextualMatchInDescriptionOrderByAvailabilityAndPricePerMinute() {
        HttpHeaders params = new HttpHeaders();
        params.add("description", "nice");
        params.add("sortCriteria", "name"); //we need sort result to assert names.
        ResponseEntity<ExpertsSearchResult> result = query(params);

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertNotNull(result.getBody());
        assertNotNull(result.getBody().getExperts());
        List<Expert> experts = result.getBody().getExperts();
        assertEquals(4, experts.size());
        assertEquals(asList("Alice_3", "MAx_3", "Oleh_3", "Philip_3"), experts.stream().map(Expert::getName).collect(toList()));
    }


    @Test
    @DisplayName("GET http://localhost:8080/experts?name=NOT_EXISTING_NAME")
    void notFoundIfEmptyList() {
        HttpHeaders params = new HttpHeaders();
        params.add("name", "NOT_EXISTING_NAME");
        ResponseEntity<ExpertsSearchResult> result = query(params);

        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
    }

    @Test
    @DisplayName("GET http://localhost:8080/experts?sortCriteria=WRONG_COLUMN_NAME")
    void wrongColumnName() {
        HttpHeaders params = new HttpHeaders();
        params.add("sortCriteria", "WRONG_COLUMN_NAME");
        ResponseEntity<ExpertsSearchResult> result = query(params);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
    }

    private ResponseEntity<ExpertsSearchResult> query() {
        String url = "http://localhost:" + port + "/experts";
        return query(url);
    }

    private ResponseEntity<ExpertsSearchResult> query(MultiValueMap<String, String> params) {
        String url = "http://localhost:" + port + "/experts";
        return query(UriComponentsBuilder.fromHttpUrl(url).queryParams(params).toUriString());
    }

    private ResponseEntity<ExpertsSearchResult> query(String url) {
        return query(url, APPLICATION_JSON);
    }

    private ResponseEntity<ExpertsSearchResult> query(String url, MediaType mediaType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(mediaType);
        headers.setAccept(Collections.singletonList(mediaType));
        return restTemplate.exchange(
                url,
                HttpMethod.GET,
                new HttpEntity(headers),
                ExpertsSearchResult.class
        );
    }
}
package net.lafox.adviqo.challenge.service;

import static java.util.Arrays.asList;
import static net.lafox.adviqo.challenge.dto.Availability.BUSY;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import net.lafox.adviqo.challenge.dto.Expert;
import net.lafox.adviqo.challenge.dto.ExpertsSearchResult;
import net.lafox.adviqo.challenge.persist.ExpertDao;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ExpertServiceImplTest {

    @Mock
    private ExpertDao expertDao;
    private ExpertService expertService;

    @BeforeEach
    void setUp() {
        expertService = new ExpertServiceImpl(expertDao);
    }

    @Test
    void fetchExperts() {
        when(expertDao.fetchExperts(any(), any())).thenReturn(
                asList(
                        Expert.builder()
                                .availability(BUSY)
                                .description("descr")
                                .language("Eng")
                                .name("name")
                                .pricePerMinute(11.0)
                                .build()
                )
        );
        ExpertsSearchResult result = expertService.fetchExperts(null, null, null, null);

        Assertions.assertNotNull(result);
        Assertions.assertNotNull(result.getExperts());
        Assertions.assertEquals(1, result.getExperts().size());
        Expert a0 = result.getExperts().get(0);
        Assertions.assertEquals(BUSY, a0.getAvailability());
        Assertions.assertEquals("descr", a0.getDescription());
        Assertions.assertEquals("Eng", a0.getLanguage());
        Assertions.assertEquals("name", a0.getName());
        Assertions.assertEquals(11.0, a0.getPricePerMinute(), 0.01);
    }
}
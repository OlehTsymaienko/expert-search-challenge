package net.lafox.adviqo.challenge.persist;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static net.lafox.adviqo.challenge.dto.Availability.BUSY;
import static net.lafox.adviqo.challenge.dto.Availability.OFFLINE;
import static net.lafox.adviqo.challenge.dto.Availability.ONLINE;
import static net.lafox.adviqo.challenge.service.ds.ComparisonType.EQUAL;
import static net.lafox.adviqo.challenge.service.ds.ComparisonType.LIKE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;

import net.lafox.adviqo.challenge.dto.Availability;
import net.lafox.adviqo.challenge.dto.Expert;
import net.lafox.adviqo.challenge.service.ds.FilterCriteria;
import net.lafox.adviqo.challenge.service.ds.SortCriteria;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;

@SpringBootTest
class ExpertDaoHibernateTest {

    @Autowired
    private ExpertDao expertDao;

    @Test
    @DisplayName("fetchExperts with NULLs")
    void fetchExpertsNullTest() {
        assertEquals(12, expertDao.fetchExperts(null, null).size());
    }

    @Test
    @DisplayName("fetchExperts with emptyLists")
    void fetchExpertsEmptyListTest() {
        assertEquals(12, expertDao.fetchExperts(emptyList(), emptyList()).size());
    }

    @Test
    @DisplayName("fetchExperts bad sort field name")
    void fetchExpertsBadFieldName() {
        Throwable exception = assertThrows(
                InvalidDataAccessApiUsageException.class,
                () -> expertDao.fetchExperts(emptyList(), asList(SortCriteria.of("BAD-FIELD-NAME")))
        );
        assertTrue(exception.getMessage().startsWith("Unable to locate Attribute  with the the given name [BAD-FIELD-NAME] on this ManagedType"));
    }

    @Test
    @DisplayName("fetchExperts orderBy test")
    void fetchExpertsOrderByTest() {
        assertPrices(
                expertDao.fetchExperts(emptyList(), asList(SortCriteria.of("pricePerMinute:asc"))),
                1.0, 1.0, 1.0, 2.0, 2.0, 2.0, 3.0, 3.0, 3.0, 4.0, 4.0, 4.0
        );

        assertPrices(
                expertDao.fetchExperts(emptyList(), asList(SortCriteria.of("pricePerMinute:desc"))),
                4.0, 4.0, 4.0, 3.0, 3.0, 3.0, 2.0, 2.0, 2.0, 1.0, 1.0, 1.0
        );

        assertAvailability(
                expertDao.fetchExperts(emptyList(), asList(SortCriteria.of("availability:desc"))),
                ONLINE, ONLINE, ONLINE, ONLINE, OFFLINE, OFFLINE, OFFLINE, OFFLINE, BUSY, BUSY, BUSY, BUSY
        );

        assertAvailability(
                expertDao.fetchExperts(emptyList(), asList(SortCriteria.of("availability:asc"))),
                BUSY, BUSY, BUSY, BUSY, OFFLINE, OFFLINE, OFFLINE, OFFLINE, ONLINE, ONLINE, ONLINE, ONLINE
        );

        assertAvailabilityAndPrice(
                expertDao.fetchExperts(emptyList(), asList(SortCriteria.of("pricePerMinute:asc"), SortCriteria.of("availability:asc"))),
                "BUSY-1.0", "OFFLINE-1.0", "ONLINE-1.0", "BUSY-2.0", "OFFLINE-2.0", "ONLINE-2.0", "BUSY-3.0", "OFFLINE-3.0", "ONLINE-3.0", "BUSY-4.0", "OFFLINE-4.0", "ONLINE-4.0"
        );

        assertAvailabilityAndPrice(
                expertDao.fetchExperts(emptyList(), asList(SortCriteria.of("availability:desc"), SortCriteria.of("pricePerMinute:asc"))),
                "ONLINE-1.0", "ONLINE-2.0", "ONLINE-3.0", "ONLINE-4.0", "OFFLINE-1.0", "OFFLINE-2.0", "OFFLINE-3.0", "OFFLINE-4.0", "BUSY-1.0", "BUSY-2.0", "BUSY-3.0", "BUSY-4.0"
        );
    }

    @Test
    @DisplayName("fetchExperts filter test")
    void fetchExpertsFilterTest() {
        List<SortCriteria> sortCriteria = asList(SortCriteria.of("pricePerMinute:asc"));

        assertName(
                expertDao.fetchExperts(asList(FilterCriteria.of("name", "Alice_1", EQUAL)), sortCriteria),
                "Alice_1"
        );

        assertName(
                expertDao.fetchExperts(asList(FilterCriteria.of("description", "nice", LIKE)), sortCriteria),
                "Philip_3", "Oleh_3", "Alice_3", "MAx_3"
        );

        assertName(
                expertDao.fetchExperts(asList(FilterCriteria.of("language", "English", LIKE)), sortCriteria),
                "Oleh_1", "Oleh_2", "Oleh_3", "MAx_1", "MAx_2", "MAx_3"
        );
    }

    private void assertPrices(List<Expert> actual, Double... expected) {
        assertEquals(asList(expected), actual.stream().map(Expert::getPricePerMinute).collect(toList()));
    }

    private void assertAvailability(List<Expert> actual, Availability... expected) {
        assertEquals(asList(expected), actual.stream().map(Expert::getAvailability).collect(toList()));
    }

    private void assertAvailabilityAndPrice(List<Expert> actual, String... expected) {
        assertEquals(asList(expected), actual.stream().map(x -> x.getAvailability() + "-" + x.getPricePerMinute()).collect(toList()));
    }

    private void assertName(List<Expert> actual, String... expected) {
        assertEquals(asList(expected), actual.stream().map(Expert::getName).collect(toList()));
    }

    private <T> List<T> asList(T... a) {
        return Arrays.asList(a);
    }
}
# Backend Developer Challenge – Spring Boot Java
### Story
As a technical lead I want to have a Search Service so that I can easily search for experts match- ing specific criteria.

### build and run (Linux/MacOS):
build
```
./mvnw clean install
```
run
```
target/challenge-0.0.1-SNAPSHOT.jar
```

### Manual Check of Acceptance Criteria:
fetch all as XML
```
curl -H "Accept: application/xml" -X GET http://localhost:8080/experts
```
fetch all as JSON
```
curl -H "Accept: application/json" -X GET http://localhost:8080/experts
```
fetch experts by name
```
curl -H "Accept: application/json" -X GET http://localhost:8080/experts?name=Alice_1
```
fetch experts by language with sorting on availability and price
```
curl -H "Accept: application/json" -X GET http://localhost:8080/experts?language=French&sortCriteria=availability:asc,pricePerMinute:desc
```
fetch experts by language with sorting on price
```
curl -H "Accept: application/json" -X GET http://localhost:8080/experts?language=French&sortCriteria=pricePerMinute
```
fetch experts by language with sorting on availability
```
curl -H "Accept: application/json" -X GET http://localhost:8080/experts?language=French&sortCriteria=availability

```
